import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import NavBar from './components/common/NavBar'
const Home = lazy(() => import('./layouts/Home'));
const Stock = lazy(() => import('./layouts/Stock'));
const Provider = lazy(() => import('./layouts/Provider'));
// const About = lazy(() => import('./routes/About'));

function App() {
  return (
    <Router>
      <NavBar>
        <Suspense fallback={<div>Loading...</div>}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/stock" component={Stock} />
            <Route path="/provider" component={Provider} />
            {/* <Route path="/about" component={About}/> */}
          </Switch>
        </Suspense>
      </NavBar>
    </Router>
  );
}

export default App;

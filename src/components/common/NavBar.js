import React from 'react';
import { NavLink } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBox, faChartBar, faBoxes, faCashRegister, faCalculator } from '@fortawesome/free-solid-svg-icons'

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: 'auto',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  inactiveLink: {
    color: '#5c5c5c',
    textDecoration: 'none'
  }
}));

export default function ClippedDrawer({ children }) {
  const classes = useStyles();
  const menuItems = [
    {
      name: 'Resumenes',
      icon: faChartBar,
      route: '/'
    },
    {
      name: 'Stock',
      icon: faBox,
      route: '/stock'
    },
    {
      name: 'Proveedor',
      icon: faBoxes,
      route: '/provider'
    },
    {
      name: 'Ventas',
      icon: faCashRegister,
      route: '/sale'
    },
    {
      name: 'Presupuesto',
      icon: faCalculator,
      route: '/budget'
    }
  ];
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" noWrap>
            Stock Control
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          <List>
            {menuItems.map((item, index) => (
              <NavLink exact to={item.route} className={classes.inactiveLink} activeStyle={{
                fontWeight: "bold",
                color: "#074668",
              }}>
                <ListItem button key={item.name}>
                  <ListItemIcon>
                    <FontAwesomeIcon icon={item.icon} size="2x" />
                  </ListItemIcon>
                  {item.name}
                </ListItem>
              </NavLink>
            ))}
          </List>
        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        {children}
      </main>
    </div>
  );
}

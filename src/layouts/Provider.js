import React from 'react';
import Table from '../components/common/Table/Table';
import axios from "../utils/api";

import Dialog from '../components/common/Dialog/Dialog';
import TextField from '@material-ui/core/TextField';

const tableModel = [
  { id: 'id', numeric: false, disablePadding: true, label: 'Proveedor N°' },
  { id: 'company_name', numeric: true, disablePadding: false, label: 'Compania' },
  { id: 'contact_name', numeric: true, disablePadding: false, label: 'Contacto' },
  { id: 'contact_number', numeric: true, disablePadding: false, label: 'Número de Contacto' },
];

const Provider = () => {

  const [appState, setAppState] = React.useState({
    loading: false,
    providers: [],
    nextPage: null,
    prevPage: null,
    totalItems: 0
  });
  const [open, setOpen] = React.useState(false);
  const [provider, setProvider] = React.useState({
    company_name: '',
    contact_name: '',
    contact_number: ''
  });


  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setProvider({
      company_name: '',
      contact_name: '',
      contact_number: ''
    });
    setOpen(false);
  };

  const handleProvider = (event) => {
    console.log(event.target.value)
    console.log(event.target.id)
    let newProvider = {
      ...provider
    };
    newProvider[event.target.id] = event.target.value
    console.log(newProvider)
    setProvider(newProvider);
    
  };

  React.useEffect(() => {
    setAppState({ loading: true });
    getProviders()
  }, [setAppState]);

  const saveProvider = () => {
    setAppState({ loading: true });
    axios.post('/provider', provider).then((res) => {
      console.log(res)
      handleClose()
      getProviders()
    });
    return true
  }

  const getProviders = (length) => {
    axios.get(`/providers/${length ? length : '5'}`).then((res) => {
      console.log(res)
      const providers = res.data.data;
      setAppState({ 
        loading: false, 
        providers: providers, 
        nextPage: res.data['next_page_url'],
        prevPage: res.data['prev_page_url'],
        totalItems:  res.data['total'] 
      });
    }).catch(function (error) {
      console.log('ERROR:', error)
      setAppState({ loading: false });
    });
  }

  return (
    <div>
      <Table
        rows={appState.providers}
        loading={appState.loading}
        title='Proveedores'
        tableModel={tableModel}
        onAddButtonClick={handleClickOpen}
        getItems={getProviders}
        totalItems={appState.totalItems}
      //searchButtonAction={}
      >
        <Dialog
          title='Nuevo Proveedor'
          open={open}
          handleClose={handleClose}
          saveButton={saveProvider}
        >
          <form>
            <TextField
              autoFocus
              margin="dense"
              id="company_name"
              label="Empresa"
              value={provider.company_name}
              onChange={handleProvider}
              fullWidth
            />
            <TextField

              margin="dense"
              id="contact_name"
              label="Nombre de contacto"
              value={provider.contact_name}
              onChange={handleProvider}
              fullWidth
            />
            <TextField

              margin="dense"
              id="contact_number"
              label="Numero de contacto"
              value={provider.contact_number}
              onChange={handleProvider}
              fullWidth
            />
          </form>
        </Dialog>
      </Table>
    </div>);

}

export default Provider;
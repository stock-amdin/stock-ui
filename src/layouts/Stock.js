import React from 'react';
import Table from '../components/common/Table/Table';
import axios from "../utils/api";

import Dialog from '../components/common/Dialog/Dialog';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';

const tableModel = [
  { id: 'code', numeric: false, disablePadding: true, label: 'Codigo' },
  { id: 'description', numeric: true, disablePadding: false, label: 'Descripcion' },
  { id: 'brand', numeric: true, disablePadding: false, label: 'Marca' },
  //TODO: write the correct label orgian_price to origianl_price
  { id: 'orgian_price', numeric: true, disablePadding: false, label: 'Precio' },
  { id: 'suggested_price', numeric: true, disablePadding: false, label: 'Precio Sugerido' },
  { id: 'price', numeric: true, disablePadding: false, label: 'Precio de Venta' },
  { id: 'gain', numeric: true, disablePadding: false, label: 'Ganancia' },
  { id: 'discount', numeric: true, disablePadding: false, label: 'Descuento' },
  { id: 'quantity', numeric: true, disablePadding: false, label: 'Cantidad' }
];
const Stock = () => {

  const [appState, setAppState] = React.useState({
    loading: false,
    products: [],
    nextPage: null,
    prevPage: null,
    totalItems: 0
  });
  const [open, setOpen] = React.useState(false);
  const [providres, setProviders] = React.useState([]);
  const [product, setProduct] = React.useState({
    code: '',
    description: '',
    brand: '',
    orgian_price: '',
    suggested_price: '',
    price: '',
    gain: '',
    discount: '',
    quantity: 0,
    id_provider: ''
  });


  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setProduct({
      code: '',
      description: '',
      brand: '',
      orgian_price: '',
      suggested_price: '',
      price: '',
      gain: '',
      discount: '',
      quantity: '',
      id_provider: ''
    });
    setOpen(false);
  };

  const handleProduct = (event) => {
    console.log(event.target.value)
    console.log(event.target.name)
    let newProvider = {
      ...product
    };
    newProvider[event.target.name] = event.target.value
    console.log(newProvider)
    setProduct(newProvider);

  };

  React.useEffect(() => {
    setAppState({ loading: true });
    getProducts();
    getProviders();
  }, [setAppState]);

  const saveProducts = () => {
    setAppState({ loading: true });
    console.log('Saving...')
    axios.post('/product', product).then((res) => {
      console.log(res)
      handleClose()
      getProducts()
    });
    return true
  }

  const getProducts = (length) => {
    axios.get(`/products/${length ? length : '5'}`).then((res) => {
      console.log(res)
      const products = res.data.data;
      setAppState({
        loading: false,
        products: products,
        nextPage: res.data['next_page_url'],
        prevPage: res.data['prev_page_url'],
        totalItems: res.data['total']
      });
    }).catch(function (error) {
      console.log('ERROR:', error)
      setAppState({ loading: false });
    });
  }

  const getProviders = () => {
    axios.get(`/providers`).then((res) => {
      console.log(res)
      const providers = res.data;
      setProviders(providers)
    }).catch(function (error) {
      console.log('ERROR:', error)
      setAppState({ loading: false });
    });
  }

  return (
    <div>
      <Table
        rows={appState.products}
        loading={appState.loading}
        title='Productos'
        tableModel={tableModel}
        onAddButtonClick={handleClickOpen}
        getItems={getProducts}
        totalItems={appState.totalItems}
      >
        <Dialog
          title='Nuevo Producto'
          open={open}
          handleClose={handleClose}
          saveButton={saveProducts}
          scroll={'body'}
        >
          <form>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <TextField
                  autoFocus
                  margin="dense"
                  id="description"
                  name="description"
                  label="Descripcion"
                  value={product.description}
                  onChange={handleProduct}
                  fullWidth
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  
                  margin="dense"
                  id="brand"
                  name="brand"
                  label="Marca"
                  value={product.brand}
                  onChange={handleProduct}
                  fullWidth
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  margin="dense"
                  select
                  id="id_provider"
                  name="id_provider"
                  label="Proveedor"
                  value={product.id_provider}
                  onChange={handleProduct}
                  fullWidth
                >
                  {providres.map((option) => (
                    <MenuItem key={option.id} value={option.id} name="id_provider">
                      {option.company_name}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid item xs={3}>
                <TextField
                  margin="dense"
                  id="orgian_price"
                  name="orgian_price"
                  label="Precio de Costo"
                  value={product.orgian_price}
                  onChange={handleProduct}
                  fullWidth
                  InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                  }}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  margin="dense"
                  id="suggested_price"
                  name="suggested_price"
                  label="Precio Sugerido"
                  value={product.suggested_price}
                  onChange={handleProduct}
                  fullWidth
                  InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                  }}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  margin="dense"
                  id="price"
                  name="price"
                  label="Precio"
                  value={product.price}
                  onChange={handleProduct}
                  fullWidth
                  InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                  }}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  margin="dense"
                  id="gain"
                  name="gain"
                  label="Ganancia"
                  value={product.gain}
                  onChange={handleProduct}
                  fullWidth
                  InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                  }}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  margin="dense"
                  id="discount"
                  name="discount"
                  label="Descuento"
                  value={product.discount}
                  onChange={handleProduct}
                  fullWidth
                  InputProps={{
                    startAdornment: <InputAdornment position="start">%</InputAdornment>,
                  }}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  margin="dense"
                  id="quantity"
                  name="quantity"
                  label="Cantidad"
                  type="number"
                  value={product.quantity}
                  onChange={handleProduct}
                  fullWidth
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  margin="dense"
                  id="code"
                  name="code"
                  label="Codigo"
                  value={product.code}
                  onChange={handleProduct}
                  fullWidth
                />
              </Grid>
              
            </Grid>


          </form>
        </Dialog>
      </Table>
    </div>);

}

export default Stock;